CREATE TABLE `appointment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `appoint_datetime` datetime NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `description` text NOT NULL,
  `is_walk_in` tinyint NOT NULL,
  `patient_id` int NOT NULL,
  `employee_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_86b3e35a97e289071b4785a1402` (`patient_id`),
  KEY `FK_cad339a11ed0408e417671162fb` (`employee_id`),
  CONSTRAINT `FK_86b3e35a97e289071b4785a1402` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`),
  CONSTRAINT `FK_cad339a11ed0408e417671162fb` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
);
CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `doctor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `specialty` varchar(255) NOT NULL,
  `salary` int NOT NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `doctor_schedule` (
  `doctor_id` int NOT NULL,
  `schedule_id` int NOT NULL,
  PRIMARY KEY (`doctor_id`, `schedule_id`),
  UNIQUE KEY `REL_4172861ac5dee91ca3f851b8f6` (`schedule_id`),
  CONSTRAINT `FK_3dbc83e2a26386a2e5065a75df8` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
  CONSTRAINT `FK_4172861ac5dee91ca3f851b8f6a` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`)
);
CREATE TABLE `employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `salary` int NOT NULL,
  `department_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d62835db8c0aec1d18a5a927549` (`department_id`),
  CONSTRAINT `FK_d62835db8c0aec1d18a5a927549` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
);
CREATE TABLE `medicine` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit_price` int NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `medicine_dispense` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `patient_visit_id` int NOT NULL,
  `medicine_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_29786f1021377a078b9e2d404a7` (`patient_visit_id`),
  KEY `FK_1ab8646de4c55c39811f8ad8bbf` (`medicine_id`),
  CONSTRAINT `FK_1ab8646de4c55c39811f8ad8bbf` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`),
  CONSTRAINT `FK_29786f1021377a078b9e2d404a7` FOREIGN KEY (`patient_visit_id`) REFERENCES `patient_visit` (`id`)
);
CREATE TABLE `medicine_prescription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `price` int NOT NULL,
  `request_datetime` datetime NOT NULL,
  `expire_date` date NOT NULL,
  `medicine_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fd84b2cbb7a990ac5efb1a0509e` (`medicine_id`),
  CONSTRAINT `FK_fd84b2cbb7a990ac5efb1a0509e` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`)
);
CREATE TABLE `patient` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `id_card` varchar(13) NOT NULL,
  `address` text NOT NULL,
  `province` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE `patient_visits` (
  `id` int NOT NULL AUTO_INCREMENT,
  `visit_datetime` datetime NOT NULL,
  `cause` text NOT NULL,
  `appointment_id` int DEFAULT NULL,
  `doctorId` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_3d4129d08b1357cd92e29284bb` (`appointment_id`),
  KEY `FK_fbe76e358b34f2ee036825df64b` (`doctorId`),
  CONSTRAINT `FK_3d4129d08b1357cd92e29284bb3` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`id`),
  CONSTRAINT `FK_fbe76e358b34f2ee036825df64b` FOREIGN KEY (`doctorId`) REFERENCES `doctor` (`id`)
);
CREATE TABLE `schedule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `day` enum('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN') NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`id`)
);