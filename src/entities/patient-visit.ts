import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Appointment } from "./appointment";
import { Doctor } from "./doctor";
import { MedicineDispense } from "./medicine-dispense";
import { Patient } from "./patient";

@Entity("patient_visit")
export class PatientVisit {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "datetime",
    name: "visit_datetime",
    nullable: false,
  })
  visitDate: Date;

  @Column({
    type: "text",
    nullable: false,
  })
  cause: string;

  @OneToOne(() => Appointment, (a) => a.patientVisited)
  @JoinColumn({
    name: "appointment_id",
  })
  appointment: Appointment;

  @OneToMany(
    () => MedicineDispense,
    (drugDispenses) => drugDispenses.patientVisited
  )
  dispenseDrugs: MedicineDispense[];

  @ManyToOne(() => Doctor, (doctor) => doctor.patientVisits, {
    nullable: false,
  })
  @JoinColumn({
    name: "doctor_id",
  })
  doctor: Doctor;
}
