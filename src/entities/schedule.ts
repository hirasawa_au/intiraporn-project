import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Doctor } from "./doctor";

export enum ScheduleDay {
  MONDAY = "MON",
  TUESDAY = "TUE",
  WEDNESDAY = "WED",
  THURSDAY = "THU",
  FRIDAY = "FRI",
  SATURDAY = "SAT",
  SUNDAY = "SUN",
}

@Entity({
  name: "schedule",
})
export class Schedule {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "enum",
    enum: ScheduleDay,
    name: "day",
    nullable: false,
  })
  appointmentDay: ScheduleDay;

  @Column({
    type: "time",
    name: "start_time",
    nullable: false,
  })
  startTime: Date;

  @Column({
    type: "time",
    name: "end_time",
    nullable: false,
  })
  endTime: Date;

  @ManyToOne(() => Doctor, (doctor) => doctor.schedules, {
    nullable: false,
  })
  @JoinColumn({
    name: "doctor_id",
  })
  doctor: Doctor;
}
