import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Appointment } from "./appointment";
import { Department } from "./department";
@Entity("employee")
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", {
    length: 255,
    nullable: false,
  })
  firstname: string;

  @Column("varchar", {
    length: 255,
    nullable: false,
  })
  lastname: string;

  @Column("int", {
    nullable: false,
  })
  salary: number;

  @ManyToOne(() => Department, (department) => department.employees, {
    nullable: false,
  })
  @JoinColumn({
    name: "department_id",
  })
  department: Department;

  @OneToMany(() => Appointment, (appointment) => appointment.employee)
  createdAppointments: Appointment[];
}
