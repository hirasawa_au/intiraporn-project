import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Medicine } from "./medicine";

@Entity()
export class MedicinePrescription {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("int", {
    nullable: false,
  })
  quantity: number;

  @Column("int", {
    nullable: false,
  })
  price: number;

  @Column("datetime", {
    nullable: false,
  })
  request_datetime: Date;

  @Column("date", {
    nullable: false,
  })
  expire_date: Date;

  @ManyToOne(() => Medicine, (m) => m.prescription)
  @JoinColumn({
    name: "medicine_id",
  })
  medicine: Medicine;
}
