import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { MedicineDispense } from "./medicine-dispense";
import { MedicinePrescription } from "./medicine-prescription";
import { PatientVisit } from "./patient-visit";

@Entity("medicine")
export class Medicine {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 255,
    nullable: false,
  })
  name: string;

  @Column("int", {
    name: "unit_price",
    nullable: false,
  })
  unitPrice: number;

  @Column({
    nullable: false,
  })
  quantity: number;

  @OneToMany(() => MedicinePrescription, (m) => m.medicine)
  prescription: MedicinePrescription[];

  @OneToMany(() => MedicineDispense, (m) => m.medicine)
  medicine_dispenses: MedicineDispense[];
}
