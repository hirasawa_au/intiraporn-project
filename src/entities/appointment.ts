import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Doctor } from "./doctor";
import { Employee } from "./employee";
import { Patient } from "./patient";
import { PatientVisit } from "./patient-visit";

@Entity("appointment")
export class Appointment {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => PatientVisit, (pv) => pv.appointment)
  patientVisited: PatientVisit;

  @Column({
    type: "datetime",
    name: "appoint_datetime",
    nullable: false,
  })
  appointDate: Date;

  @CreateDateColumn({
    name: "created_at",
    nullable: false,
  })
  createdAt: Date;

  @Column({
    type: "text",
    nullable: false,
  })
  description: string;

  @ManyToOne(() => Patient, (patient) => patient.appointments, {
    nullable: false,
  })
  @JoinColumn({
    name: "patient_id",
  })
  patient: Patient;

  @ManyToOne(() => Employee, (employee) => employee.createdAppointments, {
    nullable: false,
  })
  @JoinColumn({
    name: "employee_id",
  })
  employee: Employee;

  @Column({
    name: "is_walk_in",
    type: "bool",
    nullable: false,
  })
  isWalkIn: boolean;
}
