import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Appointment } from "./appointment";
import { PatientVisit } from "./patient-visit";
import { Schedule } from "./schedule";

@Entity("doctor")
export class Doctor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", {
    length: 255,
    nullable: false,
  })
  firstname: string;

  @Column("varchar", {
    length: 255,
    nullable: false,
  })
  lastname: string;

  @Column("varchar", {
    length: 255,
    nullable: false,
  })
  specialty: string;

  @Column("int", {
    nullable: false,
  })
  salary: number;

  @OneToMany(() => PatientVisit, (patientVisit) => patientVisit.doctor)
  patientVisits: PatientVisit[];

  @OneToMany(() => Schedule, (schedule) => schedule.doctor)
  schedules: Schedule[];
}
