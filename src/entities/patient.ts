import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  OneToMany,
} from "typeorm";
import { Appointment } from "./appointment";

@Entity("patient")
export class Patient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 255,
    nullable: false,
  })
  firstname: string;

  @Column({
    type: "varchar",
    length: 255,
    nullable: false,
  })
  lastname: string;

  @Column({
    type: "varchar",
    length: 13,
    nullable: false,
    name: "id_card",
  })
  idCard: string;

  @Column({
    type: "text",
    nullable: false,
  })
  address: string;

  @Column({
    type: "varchar",
    length: 255,
    nullable: false,
  })
  province: string;

  @OneToMany(() => Appointment, (appointment) => appointment.patient)
  appointments: Appointment[];
}
