import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Medicine } from "./medicine";
import { Patient } from "./patient";
import { PatientVisit } from "./patient-visit";

@Entity("medicine_dispense")
export class MedicineDispense {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("int", {
    nullable: false,
  })
  quantity: number;

  @ManyToOne(
    () => PatientVisit,
    (patientVisited) => patientVisited.dispenseDrugs,
    {
      nullable: false,
    }
  )
  @JoinColumn({
    name: "patient_visit_id",
  })
  patientVisited: Patient;

  @ManyToOne(() => Medicine, (m) => m.medicine_dispenses, { nullable: false })
  @JoinColumn({
    name: "medicine_id",
  })
  medicine: Medicine;
}
