import { DeepPartial } from "typeorm";
import { Conn } from "./connection";
import { Department } from "./entities/department";
import { Doctor } from "./entities/doctor";
import { Employee } from "./entities/employee";
import { Medicine } from "./entities/medicine";
import { Schedule, ScheduleDay } from "./entities/schedule";
import dayjs from "dayjs";
import { Patient } from "./entities/patient";
import { Appointment } from "./entities/appointment";

function generateSchedule(
  day: ScheduleDay,
  doctors: Doctor[]
): DeepPartial<Schedule>[] {
  return [
    {
      appointmentDay: day,
      startTime: dayjs().hour(17).minute(0).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(18).minute(0).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
    {
      appointmentDay: day,
      startTime: dayjs().hour(18).minute(0).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(19).minute(0).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
    {
      appointmentDay: day,
      startTime: dayjs().hour(19).minute(0).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(20).minute(0).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
  ];
}

function generateScheduleExtra(
  day: ScheduleDay,
  doctors: Doctor[]
): DeepPartial<Schedule>[] {
  return [
    ...generateSchedule(day, doctors),
    {
      appointmentDay: day,
      startTime: dayjs().hour(8).minute(30).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(9).minute(30).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
    {
      appointmentDay: day,
      startTime: dayjs().hour(9).minute(30).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(10).minute(30).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
    {
      appointmentDay: day,
      startTime: dayjs().hour(10).minute(30).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(11).minute(30).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
    {
      appointmentDay: day,
      startTime: dayjs().hour(11).minute(30).second(0).millisecond(0).toDate(),
      endTime: dayjs().hour(12).minute(30).second(0).millisecond(0).toDate(),
      doctor: doctors[Math.floor(Math.random() * doctors.length)],
    },
  ];
}

function randomDate(start: Date, end: Date) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  );
}

async function main() {
  const connection = await Conn.connect();
  const departmentRepository = connection.getRepository(Department);

  const department = await departmentRepository.save(
    departmentRepository.create({
      name: "Wisit Clinic 1",
    })
  );

  console.log("Department created");

  const doctorRepository = connection.getRepository(Doctor);
  const doctors = await doctorRepository.save(
    doctorRepository.create(<DeepPartial<Doctor>[]>[
      {
        firstname: "Wisit",
        lastname: "Sonpee",
        specialty: "obstetrician",
        salary: 20000,
      },
      {
        firstname: "Kobayakawa",
        lastname: "Hirasawa",
        specialty: "ophthalmologist",
        salary: 19900,
      },
      {
        firstname: "Shinpei",
        lastname: "Kobayashi",
        specialty: "obstetrician",
        salary: 20000,
      },
      {
        firstname: "Ushio",
        lastname: "Kofune",
        specialty: "Orthopedic",
        salary: 25000,
      },
    ])
  );

  console.log("Doctor created");
  const employeeRepository = connection.getRepository(Employee);
  const employees = await employeeRepository.save(
    employeeRepository.create([
      {
        firstname: "Somruay",
        lastname: "Sonpee",
        salary: 12000,
        department,
      },
      {
        firstname: "Somchai",
        lastname: "Hideko",
        salary: 9000,
        department,
      },
      {
        firstname: "Lek",
        lastname: "Mahiro",
        salary: 9999,
        department,
      },
    ])
  );
  console.log("Employee created");
  const medicineRepository = connection.getRepository(Medicine);
  const medicines = await medicineRepository.save(
    medicineRepository.create([
      {
        id: 1,
        name: "CPM 4mg",
        unitPrice: 2,
        quantity: 10000,
      },
      {
        id: 2,
        name: "Sara Paracetamol 500mg",
        unitPrice: 1,
        quantity: 200000,
      },
      {
        id: 3,
        name: "AirX Mint",
        unitPrice: 10,
        quantity: 200,
      },
      {
        id: 4,
        name: "Deolin 40mg",
        unitPrice: 15,
        quantity: 200,
      },
    ])
  );
  const scheduleRepository = connection.getRepository(Schedule);

  const schedules = await scheduleRepository.save(
    scheduleRepository.create([
      ...generateSchedule(ScheduleDay.MONDAY, doctors),
      ...generateSchedule(ScheduleDay.TUESDAY, doctors),
      ...generateSchedule(ScheduleDay.WEDNESDAY, doctors),
      ...generateSchedule(ScheduleDay.THURSDAY, doctors),
      ...generateSchedule(ScheduleDay.FRIDAY, doctors),
      ...generateScheduleExtra(ScheduleDay.SATURDAY, doctors),
      ...generateScheduleExtra(ScheduleDay.SUNDAY, doctors),
    ])
  );

  console.log("Schedule created");

  const patientRepository = connection.getRepository(Patient);
  const patients = await patientRepository.save(
    patientRepository.create([
      {
        firstname: "มิโอะ",
        lastname: "อากิยามะ",
        idCard: "1234567890123",
        address: "123/456",
        province: "สุโขทัย",
      },
      {
        firstname: "มิโอะ",
        lastname: "โคฟูเนะ",
        idCard: "1234567890124",
        address: "123/2",
        province: "สุโขทัย",
      },
      {
        firstname: "เล็ก",
        lastname: "มาฮิโร่",
        idCard: "1234567890125",
        address: "123/3",
        province: "สุโขทัย",
      },
      {
        firstname: "สมหญิง",
        lastname: "ขัตติ",
        idCard: "1234567890126",
        address: "123/4",
        province: "พิษณุโลก",
      },
      {
        firstname: "สมชาย",
        lastname: "ฮิดเกโก",
        idCard: "1234567890127",
        address: "123/5",
        province: "พิษณุโลก",
      },
      {
        firstname: "สมหญิง",
        lastname: "ซอนพี",
        idCard: "1234567890128",
        address: "123/6",
        province: "พิษณุโลก",
      },
      {
        firstname: "สมชาย",
        lastname: "ซอนพี",
        idCard: "1234567890129",
        address: "123/6",
        province: "พิษณุโลก",
      },
    ])
  );
}

main();
