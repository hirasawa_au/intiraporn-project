import { EOL } from "os";

import { Direction, Flags, Format, TypeormUml } from "typeorm-uml";

import { Conn } from "./connection";

async function build() {
  const connection = await Conn.connect();

  const flags: Flags = {
    direction: Direction.LR,
    format: Format.PNG,
    handwritten: false,
    "with-table-names-only": true,
  };

  const typeormUml = new TypeormUml();
  const url = await typeormUml.build(connection, flags);

  return process.stdout.write("Diagram URL: " + url + EOL);
}

build();
