import { DeepPartial, Repository } from "typeorm";
import { Department } from "./entities/department";
import { Doctor } from "./entities/doctor";
import { Employee } from "./entities/employee";
import { Medicine } from "./entities/medicine";
import { MedicineDispense } from "./entities/medicine-dispense";
import { MedicinePrescription } from "./entities/medicine-prescription";
import { Patient } from "./entities/patient";
import { PatientVisit } from "./entities/patient-visit";
import { Schedule } from "./entities/schedule";

export class DumpTools {
  private currentPatient = 0;
  private currentDepartment = 0;
  private currentDoctor = 0;
  private currentPatientVisit = 0;
  private currentMedicine = 0;
  private currentMedicinePrescription = 0;
  private currentMedicineDispense = 0;

  constructor(
    private readonly departmentRepository: Repository<Department>,
    private readonly employeeRepository: Repository<Employee>,
    private readonly patientRepository: Repository<Patient>,
    private readonly patientVisitRepository: Repository<PatientVisit>,
    private readonly doctorRepository: Repository<Doctor>,
    private readonly scheduleRepository: Repository<Schedule>,
    private readonly medicineRepository: Repository<Medicine>,
    private readonly medicineDispenseRepository: Repository<MedicineDispense>,
    private readonly medicinePrescriptionRepository: Repository<MedicinePrescription>
  ) {}

  async createDepartment(docs: DeepPartial<Department>): Promise<Department> {
    const department = this.departmentRepository.create(docs);
    return this.departmentRepository.save(department);
  }

  private async createPatient() {
    const currentPatient = ++this.currentPatient;
    const patient = this.patientRepository.create({
      firstname: `Patient ${currentPatient}`,
      lastname: `Fitz${currentPatient}`,
      address: this.randomString(40),
      idCard: this.randomIdCard(),
      province: this.randomString(10),
    });

    return this.patientRepository.save(patient).then(async ({ id }) => {
      await this.patientRepository.update({ id }, { id: currentPatient });
      patient.id = currentPatient;
      return patient;
    });
  }

  private async createEmployee() {
    const currentPatient = ++this.currentPatient;
    const employee = this.employeeRepository.create({
      firstname: `Employee ${currentPatient}`,
      lastname: `Fitz${currentPatient}`,
      department: {
        id: this.randomNumber(1, this.currentDepartment),
      },
      salary: this.randomNumber(10000, 100000),
    });

    return this.employeeRepository.save(employee).then(async ({ id }) => {
      await this.employeeRepository.update({ id }, { id: currentPatient });
      employee.id = currentPatient;
      return employee;
    });
  }

  private async createMedicine() {
    const currentMedicine = ++this.currentMedicine;
    const medicine = this.medicineRepository.create({
      name: `Medicine ${currentMedicine}`,
      unitPrice: this.randomNumber(100, 1000),
    });

    return this.medicineRepository.save(medicine).then(async ({ id }) => {
      await this.medicineRepository.update({ id }, { id: currentMedicine });
      medicine.id = currentMedicine;
      return medicine;
    });
  }

  private randomString(
    length: number,
    chars = "abcdefghijklmnopqrstuvwxyz0123456789"
  ): string {
    return Array.from<any, string>(
      { length },
      () => chars[Math.floor(Math.random() * chars.length)]
    ).join("");
  }

  private randomIdCard(): string {
    return this.randomString(13, "0123456789");
  }

  private randomNumber(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}
