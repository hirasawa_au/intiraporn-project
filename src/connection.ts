import { Connection } from "typeorm";
import { Appointment } from "./entities/appointment";
import { Department } from "./entities/department";
import { Doctor } from "./entities/doctor";
import { Employee } from "./entities/employee";
import { MedicineDispense } from "./entities/medicine-dispense";
import { Medicine } from "./entities/medicine";
import { PatientVisit } from "./entities/patient-visit";
import { Patient } from "./entities/patient";
import { Schedule } from "./entities/schedule";
import { MedicinePrescription } from "./entities/medicine-prescription";

export const Conn = new Connection({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "1234",
  database: "keeplearning",
  synchronize: true,
  entities: [
    Appointment,
    Department,
    Doctor,
    Employee,
    MedicineDispense,
    Medicine,
    MedicinePrescription,
    PatientVisit,
    Patient,
    Schedule,
  ],
});
