-- MySQL dump 10.13  Distrib 8.0.32, for macos12.6 (arm64)
--
-- Host: 127.0.0.1    Database: intiraporn
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `appointment_datetime` datetime NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `description` text NOT NULL,
  `is_walk_in` tinyint NOT NULL,
  `patient_id` int NOT NULL,
  `employee_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_86b3e35a97e289071b4785a1402` (`patient_id`),
  KEY `FK_cad339a11ed0408e417671162fb` (`employee_id`),
  CONSTRAINT `FK_86b3e35a97e289071b4785a1402` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`),
  CONSTRAINT `FK_cad339a11ed0408e417671162fb` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
INSERT INTO `appointment` VALUES (1,'2022-08-12 18:00:00','2022-08-12 18:00:00.000000','',1,1,2),(2,'2022-08-12 18:25:00','2022-08-12 18:25:00.000000','',1,2,2),(3,'2022-08-13 17:44:22','2022-08-11 17:44:22.000000','',1,3,3),(4,'2022-08-14 17:55:22','2022-08-14 17:55:22.000000','',1,4,1),(5,'2023-09-12 19:00:00','2022-08-14 18:01:22.000000','(นัดหมาย) ตรวจครรภ์ครั้งที่ 2',0,4,1),(6,'2023-10-16 19:00:00','2022-08-14 18:01:22.000000','(นัดหมาย) ตรวจครรภ์ครั้งที่ 3',0,4,1);
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
INSERT INTO `department` VALUES (1,'Wisit Clinic 1');
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `specialty` varchar(255) NOT NULL,
  `salary` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
INSERT INTO `doctor` VALUES (1,'Wisit','Sonpee','obstetrician',20000),(2,'Kobayakawa','Hirasawa','ophthalmologist',19900),(3,'Shinpei','Kobayashi','obstetrician',20000),(4,'Ushio','Kofune','Orthopedic',25000);
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `salary` int NOT NULL,
  `department_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d62835db8c0aec1d18a5a927549` (`department_id`),
  CONSTRAINT `FK_d62835db8c0aec1d18a5a927549` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
INSERT INTO `employee` VALUES (1,'Somruay','Sonpee',12000,1),(2,'Somchai','Hideko',9000,1),(3,'Lek','Mahiro',9999,1);
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medicine` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit_price` int NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
INSERT INTO `medicine` VALUES (1,'CPM 4mg',2,10000),(2,'Sara Paracetamol 500mg',1,200000),(3,'AirX Mint',10,200),(4,'Deolin 40mg',15,199),(5,'Bayer Aspirin 81mg',1,300000),(6,'OREDA rehydration salt',8,100);
UNLOCK TABLES;

--
-- Table structure for table `medicine_dispense`
--

DROP TABLE IF EXISTS `medicine_dispense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medicine_dispense` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `patient_visit_id` int NOT NULL,
  `medicine_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_29786f1021377a078b9e2d404a7` (`patient_visit_id`),
  KEY `FK_1ab8646de4c55c39811f8ad8bbf` (`medicine_id`),
  CONSTRAINT `FK_1ab8646de4c55c39811f8ad8bbf` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`),
  CONSTRAINT `FK_29786f1021377a078b9e2d404a7` FOREIGN KEY (`patient_visit_id`) REFERENCES `patient_visit` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine_dispense`
--

LOCK TABLES `medicine_dispense` WRITE;
INSERT INTO `medicine_dispense` VALUES (1,20,1,2),(2,10,2,5),(3,4,3,6),(4,10,3,2),(5,1,3,4);
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `medicine_dispense_trigger` AFTER INSERT ON `medicine_dispense` FOR EACH ROW BEGIN
  UPDATE medicine
  SET quantity = quantity - NEW.quantity
  WHERE id = NEW.medicine_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `medicine_prescription`
--

DROP TABLE IF EXISTS `medicine_prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medicine_prescription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `price` int NOT NULL,
  `request_datetime` datetime NOT NULL,
  `expire_date` date NOT NULL,
  `medicine_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fd84b2cbb7a990ac5efb1a0509e` (`medicine_id`),
  CONSTRAINT `FK_fd84b2cbb7a990ac5efb1a0509e` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine_prescription`
--

LOCK TABLES `medicine_prescription` WRITE;
INSERT INTO `medicine_prescription` VALUES (1,10000,1211,'2022-08-10 08:12:35','2027-06-01',2),(2,100,565,'2022-08-11 07:46:10','2027-04-23',3),(3,100,1380,'2022-08-11 12:11:00','2026-02-01',4);
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `medicine_prescription_trigger` AFTER INSERT ON `medicine_prescription` FOR EACH ROW BEGIN
  UPDATE medicine
  SET quantity = quantity + NEW.quantity
  WHERE id = NEW.medicine_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `id_card` varchar(13) NOT NULL,
  `address` text NOT NULL,
  `province` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
INSERT INTO `patient` VALUES (1,'มิโอะ','อากิยามะ','1234567890123','123/456','สุโขทัย'),(2,'มิโอะ','โคฟูเนะ','1234567890124','123/2','สุโขทัย'),(3,'เล็ก','มาฮิโร่','1234567890125','123/3','สุโขทัย'),(4,'สมหญิง','ขัตติ','1234567890126','123/4','พิษณุโลก'),(5,'สมชาย','ฮิดเกโก','1234567890127','123/5','พิษณุโลก'),(6,'สมหญิง','ซอนพี','1234567890128','123/6','พิษณุโลก'),(7,'สมชาย','ซอนพี','1234567890129','123/6','พิษณุโลก');
UNLOCK TABLES;

--
-- Table structure for table `patient_visit`
--

DROP TABLE IF EXISTS `patient_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient_visit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `visit_datetime` datetime NOT NULL,
  `cause` text NOT NULL,
  `appointment_id` int DEFAULT NULL,
  `doctor_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_db83909bdde1d2bdc42bae28ca` (`appointment_id`),
  KEY `FK_5cb301f134c298db658f3c65f42` (`doctor_id`),
  CONSTRAINT `FK_5cb301f134c298db658f3c65f42` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
  CONSTRAINT `FK_db83909bdde1d2bdc42bae28ca0` FOREIGN KEY (`appointment_id`) REFERENCES `appointment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_visit`
--

LOCK TABLES `patient_visit` WRITE;
INSERT INTO `patient_visit` VALUES (1,'2022-08-12 18:00:00','ปวดหัว ตัวร้อน ไข้ขึ้นสูง',1,3),(2,'2022-08-12 18:25:00','เหนื่อยง่าย หัวใจเต้นเร็วมาก',2,3),(3,'2022-08-13 17:44:22','ท้องเสีย เพิ่งกินซาซิมิแซลม่อนมา ตัวร้อนเล็กน้อย',3,3),(5,'2022-08-14 17:55:22','ต้องการตรวจลูกในท้อง และต้องการคำปรึกษาเกี่ยวกับการตั้งครรภ์',4,1);
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `day` enum('MON','TUE','WED','THU','FRI','SAT','SUN') NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `doctor_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bab091ad4033b47e7aaa59bbc6f` (`doctor_id`),
  CONSTRAINT `FK_bab091ad4033b47e7aaa59bbc6f` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
INSERT INTO `schedule` VALUES (1,'MON','17:00:00','18:00:00',2),(2,'MON','18:00:00','19:00:00',3),(3,'MON','19:00:00','20:00:00',4),(4,'TUE','17:00:00','18:00:00',3),(5,'TUE','18:00:00','19:00:00',1),(6,'TUE','19:00:00','20:00:00',2),(7,'WED','17:00:00','18:00:00',3),(8,'WED','18:00:00','19:00:00',4),(9,'WED','19:00:00','20:00:00',1),(10,'THU','17:00:00','18:00:00',2),(11,'THU','18:00:00','19:00:00',3),(12,'THU','19:00:00','20:00:00',1),(13,'FRI','17:00:00','18:00:00',3),(14,'FRI','18:00:00','19:00:00',1),(15,'FRI','19:00:00','20:00:00',3),(16,'SAT','17:00:00','18:00:00',2),(17,'SAT','18:00:00','19:00:00',1),(18,'SAT','19:00:00','20:00:00',1),(19,'SAT','08:30:00','09:30:00',1),(20,'SAT','09:30:00','10:30:00',1),(21,'SAT','10:30:00','11:30:00',4),(22,'SAT','11:30:00','12:30:00',1),(23,'SUN','17:00:00','18:00:00',3),(24,'SUN','18:00:00','19:00:00',1),(25,'SUN','19:00:00','20:00:00',2),(26,'SUN','08:30:00','09:30:00',3),(27,'SUN','09:30:00','10:30:00',4),(28,'SUN','10:30:00','11:30:00',1),(29,'SUN','11:30:00','12:30:00',2);
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-13 17:26:21
